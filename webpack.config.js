var path = require("path");
var basedir = path.join(__dirname, "src");
module.exports = {
  context: __dirname,
  entry: {
    script: __dirname+"/src/client/index",
    worker: __dirname+"/src/worker/index"
  },
  output: {
      path: path.join(__dirname, ".build","www","js"),
      filename: "[name].js"
  },
  resolve: {
    alias:{
      client: path.join(basedir,"client"),
      shared:  path.join(basedir,"shared"),
      worker:  path.join(basedir,"worker")
    }
  },
  plugins: [],
  module: {
    loaders: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: "babel"
        }
    ]
  }
};
