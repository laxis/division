this is a (rough) replica of the Division menu UI.

# Architecture
the general architecture is split up in several parts.:

## Client
  this is the 'UI Thread', that runs in the browser.   It is responsible for displaying the state and the sort.  

  can use `client` and `shared` script folders.

## Worker
  the worker handles updating and managing the state and the 'main logic' in our application.  
  it should be used for most functionality, so the client can stay responsive to user input.


  can use `worker` and `shared` script folders.  

## Server
  because this app has no real multi-user functionality it is able to live completely in the browser.  
  Due to this, the server's only job is to pre-render the app with the initial state and pass over to the browser.


  can use `server` and `shared` script folders.

# File Structure
## Frontend
  this directory houses all 'static' files for our app. that includes the HTML shell, all assets and the scss.

## src
  houses all script files.  
  due to some trickery (webpack aliases and [app-module-path](https://www.npmjs.com/package/app-module-path) to be correct) you can require all files by absolute values without all the relative path fuckery.


### src/client
  `require("client/<scriptname>")`  
  all files that NEED to run in the main thread go here.  this is basically everything DOM related.

### src/worker
  `require("worker/<scriptname>")`  
  self explanatory. the worker scripts go here.

### src/server
 while this probably seems weird to have with all the browser scripts, there exists a reason for that.<br> because we pre-render our page on the server, we want to have access to the components themselves (duh) and the initial state.<br> having the server files here saves us a log of file-pushing that just isn't needed now.

### src/shared
 `require("shared/<scriptname>")`  
 the biggest directory for a reason. every script that is not specific to a single part of our architecture lays here. this allows for easy sharing of common functionality.  
