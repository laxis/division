"use strict";
const gulp = require("gulp");
const util = require("util");
const path = require("path");
const gutil = require("gulp-util");
const sourcemaps = require("gulp-sourcemaps");
const webpack = require("webpack");
const webpackConfig = require("./webpack.config.js");
const babel = require('gulp-babel');

const rename = require("gulp-rename");
const svgstore = require('gulp-svgstore');
const svgmin = require('gulp-svgmin');
const inject = require('gulp-inject');
const scss = require("gulp-sass");
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const mqpacker = require('css-mqpacker');
const csswring = require('csswring');




const config = {
    src: "./frontend/",
    dist: "./.build/",
    wwwDist: "./.build/www/",
    jsDir: "./src/",
    scssDir: "./frontend/scss/"
};


gulp.task("build:server:core", function(){
  gulp.src(config.jsDir+"server/**/*.js")
  .pipe(babel())
  .pipe(gulp.dest(config.dist+"server"));
});
gulp.task("build:server:shared", function(){
  gulp.src(config.jsDir+"shared/**/*.js")
  .pipe(babel())
  .pipe(gulp.dest(config.dist+"shared"));
});

gulp.task("build:client", function(callback) {
	// modify some webpack config options
	var config = Object.create(webpackConfig);
	config.plugins = config.plugins.concat(
		new webpack.optimize.DedupePlugin()
		//, new webpack.optimize.UglifyJsPlugin()
	);

	// run webpack
	webpack(config, function(err, stats) {
		if(err) throw new gutil.PluginError("webpack:build", err);
		callback();
	});
});

gulp.task("build:css", function(){
  let processors = [
        autoprefixer({browsers: ['last 1 version']}),
        mqpacker,
        csswring
    ];
  gulp.src(config.scssDir+"*.scss")
    .pipe(scss())
    .pipe(postcss(processors))
    .pipe(gulp.dest(config.wwwDist+"css"));
});

gulp.task("move:assets", function(){

  gulp.src(config.src+"fonts/**/*")
  .pipe(gulp.dest(config.wwwDist+"fonts/"));
  
  return gulp.src(config.src+"assets/**/*")
  .pipe(gulp.dest(config.wwwDist+"assets/"));

});

gulp.task('build:svg', function () {
  return gulp.src(config.src+"assets/*.svg")
    .pipe(svgmin(function (file) {
      let prefix = path.basename(file.relative, path.extname(file.relative));
      return {
        plugins: [{
          cleanupIDs: {
            prefix: prefix + '-',
            minify: true
          }
        }]
      };
    }))
    .pipe(svgstore())
    .pipe(rename("sprite.svg"))
    .pipe(gulp.dest(config.wwwDist+"assets"));
});

gulp.task("build:html", ["build:svg"], function(){
  function fileContents (filePath, file) {
    return file.contents.toString();
  }
  return gulp
    .src(config.src+"shell.html")
    .pipe(inject(gulp.src(config.wwwDist+"assets/sprite.svg"), { transform: fileContents, removeTags: true }))
    .pipe(gulp.dest(config.wwwDist));
});

gulp.task("watch", function(){
  gulp.watch(config.scssDir+"**/*", ["build:css"]);
  gulp.watch(config.src+"*.html", ["build:html"]);
  gulp.watch(config.src+"assets/*.svg", ["build:svg"]);
  gulp.watch([config.jsDir+"client/**/*", config.jsDir+"worker/**/*"], ["build:client"]);
  gulp.watch(config.jsDir+"server/**/*",["build:server:core"]);
  gulp.watch(config.jsDir+"shared/**/*",["build:server:shared", "build:client"]);
});

gulp.task("default",["build:svg", "build:html", "move:assets", "build:client", "build:css", "build:server:core", "build:server:shared","watch"]);
gulp.task("build",["build:svg", "build:html", "move:assets", "build:css", "build:server:core", "build:server:shared", "build:client"]);
