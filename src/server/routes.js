const render = require("./renderComponent");
const {MENUSTATES} = require("shared/actions");
const express = require("express");
var router = express.Router();

/**
 * a menustate is in the format of
 * INVENTORY: "/inventory"
 * where INVENTORY is the name of a component
 * and /inventory will be used as url
 */
for(var key in MENUSTATES){

  var value = MENUSTATES[key];
  var url = value[0]==="/"?value.toLowerCase():"/"+value.toLowerCase();
  router.get(url, render.getHandler(key, url));
}

module.exports = router;
