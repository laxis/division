const path = require("path");
require('app-module-path').addPath(path.join(__dirname,"../"));

const express = require("express");
const bodyParser = require('body-parser');
const serveStatic = require("serve-static");
const config = require("shared/config");
const server = require("http").createServer();
const SocketServer = require("ws").Server;
const wss = new SocketServer({server:server});
const app = express();
const handleSocket = require("./handleSocket");
const componentRoutes = require("./routes");


wss.on("connection", handleSocket);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(serveStatic(path.join(__dirname, "../www")));
app.use(componentRoutes);
app.use("/log", function(req,res){
  console.log(req.body);
  res.status(200).json({status: 200, msg: "ok"});
});

server.on("request", app);
server.listen(config.server.port, function(){
  console.log("app listening on port", config.server.port);
});
