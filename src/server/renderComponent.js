"use strict";
const toHTML = require('mithril-node-render');
const Menu = require("shared/components/Menu");
const config = require("shared/config");
const fs = require("fs");
const path = require("path");
const initialState = require("shared/initialState");
const getComponent = require("shared/getComponent");
const m = require("mithril");

/* the base html file with placeholders */
let dryHtml = fs.readFileSync("./www/shell.html").toString();

/* this  hydrates the html with our component*/
function render({html, title, state}){
  return dryHtml
    .replace("<!--TITLE-->", title || config.title)
    .replace("<!--APP-->", html)
    .replace("<!--STATE-->", JSON.stringify(state));
}

function handle(req, res){
  var component = m.component(Menu,{dispatch: function(){}, state: initialState});
  let page = render({
    html: toHTML(component),
    title: config.title,
    state: initialState
  });
  res.send(page);

}
function getHandler(menustate, url){
  return function(req, res){
    var state = Object.assign({}, initialState, {menustate: url});
    var component = m.component(getComponent(menustate),{dispatch: function(){}, state: state});
    let page = render({
      html: toHTML(component),
      title: config.title,
      state: state
    });
    res.send(page);
  };
}
module.exports = handle;
module.exports.getHandler = getHandler;
