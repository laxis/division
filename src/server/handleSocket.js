"use strict";
const curry = require("../shared/helpers").curry;
const url = require("url");

let onMessage = curry(function onMessage(socket, event){
  console.log(event);
  let action = JSON.parse(event.data);
  setInterval(function(){
    socket.send(JSON.stringify({
      _is: "log",
      msg: Math.random()
    }));
  }, 1000);
});

module.exports = function(socket){
  var location = url.parse(socket.upgradeReq.url, true);

  // you might use location.query.access_token to authenticate or share sessions
  // or socket.upgradeReq.headers.cookie (see http://stackoverflow.com/a/16395220/151312)

  socket.on("message", onMessage(socket));
};
