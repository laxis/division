"use strict";

/**
 * dumping ground for helper functions.
 * it has a curry, as well as some haskelly helpers
 */


//  ####################
//  ## MONKEY PATCHES ##
//  ####################

Function.prototype.curry = function(f) {
    if (arguments.length<1) {
        return this; //nothing to curry with - return function
    }
    var __method = this;
    var args = Array.prototype.slice.call(arguments);
    return function() {
        return __method.apply(this, args.concat(Array.prototype.slice.call(arguments)));
    };
};
Promise.prototype.map = Promise.prototype.then;
Promise.of = Promise.resolve;



//  ################
//  ## THE M WORD ##
//  ################

const Identity = module.exports.Identity = function(value){
  this.join = () => value;
  this.map = (f) => Identity.of((f(value)));
};
Identity.of = (v) => new Identity(v);

const Nothing = module.exports.Nothing = {
  join: () => Nothing,
  map: () => Nothing,
  isNothing: () => true,
  toString: () => "Nothing",
  toJSON: () => null
};
const Just = module.exports.Just = function Just(value){
  this.join =  () => value;
  this.map =  (f) => Maybe.of(f(value));
  this.flatMap = (f) => Maybe.of(f(value).join());
  this.isNothing =  () => false;
  this.toString =  () => "Just("+value+")";
  this.toJSON =  () => value;
};
Just.of = (v) => new Just(v);

const Maybe = module.exports.Maybe = function Maybe(value){
  if(value == null){
    return Nothing;
  }
  else{
    return Just.of(value);
  }
};
Maybe.of = (v) => Maybe(v);

const Left = module.exports.Left = function Left(err){
  this.join =  () => err;
  this.map = () => Left.of(err);
  this.toJSON = () => err;
  this.toString = () => "Left("+err+")";
};
Left.of = (v) => new Left(v);
const Right = module.exports.Right = function Right(value){
  this.join =  () => value;
  this.map = (f) => Right.of(f(value));
  this.toJSON = () => value;
  this.toString = () => "Right("+value+")";
};
Right.of = (v) => new Right(v);

const either = module.exports.either = function either(f, g, m){
  switch(true){
    case (m.constructor === Left): return f(m.join());
    case (m.constructor === Right): return g(m.join());
  }
}.curry();

const IO = function(f){
  this.unsafePerformIO = f;
  this.join = this.unsafePerformIO();
  this.map = (g) => new IO(compose(f,this.unsafePerformIO));
};
IO.of = (v) => new IO(() => v);



//  ###############
//  ## FUNCTIONS ##
//  ###############

const log = module.exports.log = (e) => new IO(()=>{console.log(e); return e;});
const join = module.exports.join = (v) => v.join();
const compose = module.exports.compose = function(f,g){ return (v) => f(g(v));}.curry();
const map = module.exports.map = function(f, m){ return m.map(f);}.curry();
const flatMap = module.exports.flatMap = function(f, m){ return m.map(f).join();}.curry();

const pad = module.exports.pad =  function(str, total, filler){
  return (filler.toString().repeat(total)+str).substr(-total);
};

const curry = module.exports.curry = (function(){
  //not mine :)
  var slice = Array.prototype.slice;
  var toArray = function(a){ return slice.call(a); };
  var tail = function(a){ return slice.call(a, 1); };

  var createFn = function(fn, args, totalArity){
      var remainingArity = totalArity - args.length;

      switch (remainingArity) {
          case 0: return function(){ return processInvocation(fn, concatArgs(args, arguments), totalArity); };
          case 1: return function(a){ return processInvocation(fn, concatArgs(args, arguments), totalArity); };
          case 2: return function(a,b){ return processInvocation(fn, concatArgs(args, arguments), totalArity); };
          case 3: return function(a,b,c){ return processInvocation(fn, concatArgs(args, arguments), totalArity); };
          case 4: return function(a,b,c,d){ return processInvocation(fn, concatArgs(args, arguments), totalArity); };
          case 5: return function(a,b,c,d,e){ return processInvocation(fn, concatArgs(args, arguments), totalArity); };
          case 6: return function(a,b,c,d,e,f){ return processInvocation(fn, concatArgs(args, arguments), totalArity); };
          case 7: return function(a,b,c,d,e,f,g){ return processInvocation(fn, concatArgs(args, arguments), totalArity); };
          case 8: return function(a,b,c,d,e,f,g,h){ return processInvocation(fn, concatArgs(args, arguments), totalArity); };
          case 9: return function(a,b,c,d,e,f,g,h,i){ return processInvocation(fn, concatArgs(args, arguments), totalArity); };
          case 10: return function(a,b,c,d,e,f,g,h,i,j){ return processInvocation(fn, concatArgs(args, arguments), totalArity); };
      }
  };

  var concatArgs = function(args1, args2){
      return args1.concat(toArray(args2));
  };
  var trimArrLength = function(arr, length){
      if ( arr.length > length ) return arr.slice(0, length);
      else return arr;
  };
  var processInvocation = function(fn, argsArr, totalArity){
      argsArr = trimArrLength(argsArr, totalArity);

      if ( argsArr.length === totalArity ) return fn.apply(null, argsArr);
      return createFn(fn, argsArr, totalArity);
  };
  var curry = function(fn){
      return createFn(fn, [], fn.length);
  };
  curry.to = curry(function(arity, fn){
      return createFn(fn, [], arity);
  });
  return curry;
}());
