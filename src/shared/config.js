/**
 * here we can set all non-state 'config' stuff.
 * right now it only houses server-side stuff
 */

module.exports = {
  title: "DivisionUI",
  server:{
    port: 9000
  }
};
