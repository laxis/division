/**
 * this returns the parent of the current state
 */

const {MENUSTATES} = require("./actions");

function getParent(state){
	switch(state){
		case MENUSTATES.INTEL:
		case MENUSTATES.GROUP:
		case MENUSTATES.MAP:
		case MENUSTATES.ABILITIES:
		case MENUSTATES.INVENTORY:
		case MENUSTATES.NEWS:
		case MENUSTATES.SETTINGS:
			return MENUSTATES.MAIN;
		default:
			return MENUSTATES.MAIN;
	}
}

module.exports = getParent;
