let colors = {
  orange: "#ee8624",
  green: "#89d84e",
  blue: "#4995ed",
  lightBlue: "#68989e",
  darkGray: "#6e756e"
};

colors.getColor = function(type){
  switch(type){
    case "medical": return colors.green;
    case "tech": return colors.orange;
    case "security": return colors.blue;
    default: return colors.darkGray;
  }
};

module.exports = colors;
