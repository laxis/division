const m = require("mithril");

function getIcon({name, size, color}){
  return (<svg fill={color} className={`icon icon-${name} icon--${size}`} viewBox="0 0 64 64"><use href={"#"+name}></use></svg>);
}

module.exports = getIcon;
