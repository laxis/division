/**
 * this is the base structure of our state object.
 * it contains ALL variable data in our app.
 */


const MENUSTATES = require("./actions").MENUSTATES;

module.exports = {
  menustate: MENUSTATES.MAIN,
  settings: {
  	"threeD": true
  },
  game: {
    intelTotal: 43,
    intelFound: 2,
    position: "Pennsylvania Plaza",
    social:{
      groupMembers: 2,
      groupOwner: "LaxisB",
      friendsOnline: 0
    },
    intel: {
      total: 43,
      found: 2
    },
    base:{
      medical: 0,
      tech: 0,
      security: 0
    },
    supplies:{
      medical: 60,
      tech: 0,
      security: 0
    },
    missions: {
      current: {
        name: "Madison Field Hospital",
        distance: 62,
        type: "medical"
      }
    }
  }
};
