/**
 * this function decides which component gets rendered for each menustate.
 *
 * TODO: merge data with config.js
 */


//Components
const Menu = require("shared/components/Menu");
const Settings = require("shared/components/Settings");
const {MENUSTATES, ACTIONS} = require("shared/actions");


function getComponent(state){
  switch(state.menustate){
    case MENUSTATES.MENU:
      return Menu;
    case MENUSTATES.SETTINGS:
      return Settings;
    default:
      return Menu;
  }
}

module.exports = getComponent;
