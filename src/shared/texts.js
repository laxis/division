let groupMembers = function(number){
  return `${number} Member${number==1?"":"s"} in Group`;
};

let friendsOnline = function(number){
  return `${number} Friend${number==1?"":"s"} Online`;
};

module.exports = {
  groupMembers,
  friendsOnline
};
