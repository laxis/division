const m = require("mithril");
const colors = require("shared/colors");
const getIcon = require("shared/icon");
const {pad} = require("shared/helpers");
const log = require("shared/log").child("header");

const view = function(ctrl, props){
  return (
    <div className="header">
      <div className="row">
        <div className="player-position">
          <span className="player-position__icon">
            {getIcon({
              name: "dot",
              size: "medium",
              color: colors.orange
            })}
          </span>
          <span className="player-position__text">
            Currently Exploring {props.position}
          </span>
        </div>
      </div>
      <div>
        <div className="row base-info">
          <div className="base-info-medical col s4">
          {getIcon({
            name: "medical",
            size: "small",
            color: colors.green
          })}
          <span className="completion">{pad(props.base.medical, 2, "0")}% </span>
          <span className="supplies">
            Medical Wing supplies
            <span className="supplies-count">
              {pad(props.supplies.medical, 2, "0")}
            </span>
          </span>
          </div>
          <div className="base-info-tech col s4">
          {getIcon({
            name: "tech",
            size: "small",
            color: colors.orange
          })}
          <span className="completion">{pad(props.base.tech, 2, "0")}% </span>
          <span className="supplies">
            Tech Wing supplies
            <span className="supplies-count">
              {pad(props.supplies.tech, 2, "0")}
            </span>
          </span>
          </div>
          <div className="base-info-security col s4">
          {getIcon({
            name: "security",
            size: "small",
            color: colors.blue
          })}
          <span className="completion">{pad(props.base.security, 2, "0")}% </span>
          <span className="supplies">
            Medical Wing supplies
            <span className="supplies-count">
              {pad(props.supplies.medical, 2, "0")}
            </span>
          </span>
          </div>
        </div>
      </div>
    </div>
  );
};

module.exports = {
  view
};
