/**
 * a simple stateless progressbar
 */

const m = require("mithril");

function controller(props){
  this.getWidth = function(){
    return ((props.value/props.max)*100 | 0)+"%";
  };
}
function view(ctrl, props){
  return (
    <div className="progressbar">
      <div className="progressbar__indicator" style={ "width: "+ ctrl.getWidth()}></div>
    </div>
  );
}

module.exports = {
  controller,
  view
};
