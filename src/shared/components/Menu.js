"use strict";
/**
 * the main menu
 */

const m = require("mithril");
const MENUSTATES = require("../actions.js").MENUSTATES;
const Header = require("shared/components/partials/Header");
const Intel = require("shared/components/tiles/Intel");
const Group = require("shared/components/tiles/Group");
const Map = require("shared/components/tiles/Map");

function enter(el){
  return new Promise(function(resolve){
    resolve();
  });
}

function exit(el){
  return new Promise(function(resolve){
    resolve();
  });
}

const controller = function(props){
  this.openMenu = function(action){
    props.dispatch({type: action});
  };
  this.getClasses = function(){
    return [
      "container",
      "menu",
      props.state.settings.threeD?"menu--3d":null
    ].join(" ");
  };
  this.test = ()=>alert("ok");
};
/** @jsx m */
const view = (ctrl, props) => (
  <div className={ctrl.getClasses()}>
    <Header
      position={props.state.game.position}
      base={props.state.game.base}
      supplies={props.state.game.supplies}
    />
    <div className="row">
      <div className="col s12 m4">
      <Intel
        onclick={ctrl.openMenu.bind(null, MENUSTATES.INTEL)}
        {...props.state.game.intel} />
      </div>
      <div className="col s12 m4">
        <Group onclick={ctrl.openMenu.bind(null, MENUSTATES.GROUP)}
          {...props.state.game.social} />
      </div>
      <div className="col s12 m4">
        <Map onclick={ctrl.openMenu.bind(null, MENUSTATES.MAP)}
          {...props.state.game.missions.current}
        />
      </div>
    </div>

    <div className="row">
      <div className="col s12 m4">
        <div className="tile tile--has-content" onclick={ctrl.openMenu.bind(null, MENUSTATES.ABILITIES)}>
          <div className="tile-header">Abilities</div>
          <div className="tile-content">none</div>
        </div>
      </div>
      <div className="col s12 m4">
        <div className="tile tile--has-content" onclick={ctrl.openMenu.bind(null, MENUSTATES.INVENTORY)}>
          <div className="tile-header">Inventory</div>
          <div className="tile-content">none</div>
        </div>
      </div>
      <div className="col s12 m4">
        <div className="tile tile--has-content" onclick={ctrl.openMenu.bind(null, MENUSTATES.NEWS)}>
          <div className="tile-header">News</div>
          <div className="tile-content">None</div>
        </div>
      </div>
    </div>

    <div className="row">
      <div className="col s12 m4">
        <div className="tile tile--disabled">
          <div className="tile-header">not available in Beta</div>
        </div>
      </div>
      <div className="col s12 m4">
        <div className="tile tile--disabled">
          <div className="tile-header">not available in Beta</div>
        </div>
      </div>
      <div className="col s12 m4">
        <div className="tile tile--has-child" onclick={ctrl.openMenu.bind(null, MENUSTATES.SETTINGS)}>
          <div className="tile-header">Settings</div>
        </div>
      </div>
    </div>
  </div>);

const component = {
    controller,
    view,
    enter,
    exit
};

module.exports = component;
