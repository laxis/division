const m = require("mithril");

const Progressbar = require("shared/components/utils/Progressbar");

function view(ctrl, props){
  return (
  <div className="tile tile--intel tile--has-content" onclick={props.onclick}>
    <div className="tile-header">Intel</div>
    <div className="tile-content">
      <span>Total Progression</span>
      <span className="float-right">{props.found}/{props.total}</span>
      <Progressbar value={props.found} max={props.total} />
    </div>
  </div>
  );

}

module.exports = {
  view
};
