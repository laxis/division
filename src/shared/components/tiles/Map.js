const m = require("mithril");
const getIcon = require("shared/icon");
const colors = require("shared/colors");

function getMarker(mission){
  return (
    <div className="map__current-mission">
      <span className="current-mission__icon">
        {getIcon({
          name: mission.type,
          size: "small",
          color: colors.getColor(mission.type)
        })}
      </span>
      <span className="current-mission__text">
        <span className="mission__distance">
          {mission.distance} m
        </span>
        <span className={`mission__desc mission--${mission.type}`}>Mission</span>
      </span>
    </div>
  );
};

function view(ctrl, props){
  return (
  <div className="tile tile--map tile--has-content" onclick={props.onclick}>
    <div className="tile-header">Map</div>
    <div className="tile-content">
      <h4 className="map__mission-name">{props.name}</h4>
      {getMarker(props)}
    </div>
  </div>
  );

}

module.exports = {
  view
};
