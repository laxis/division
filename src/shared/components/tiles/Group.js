const m = require("mithril");
const getIcon = require("shared/icon");
const texts = require("shared/texts");
const colors = require("shared/colors");

function view(ctrl, props){
  return (
  <div className="tile tile--group tile--has-content" onclick={props.onclick}>
    <div className="tile-header">Group Management</div>
    <div className="tile-content row row--small">
      <div className="row row--small">
        <div className="col s4">
          <div className="group__members-icons">
            {getIcon({name: "smalldot", size: "small", color: colors.orange})}
            {Array.apply(null,Array(props.groupMembers)).map(() => getIcon({
              name: "smalldot",
              size: "smallest",
              color: colors.lightBlue
            }))}
            {Array.apply(null,Array(3-props.groupMembers)).map(() => getIcon({
              name: "smalldot",
              size: "smallest",
              color: colors.darkGray
            }))}
          </div>
        </div>
        <div className="group__text col s8 divider-left">
          <div clssName="group__members-text">{
            texts.groupMembers(props.groupMembers)
          }</div>
          <div className="group__friends">{
            texts.friendsOnline(props.friendsOnline)
          }</div>
        </div>
      </div>
      <div className="divider-top group__owner">
        <div className="text-center subheader">
          Session Owner
        </div>
        <div className="text-center">
          {props.groupOwner}
        </div>
      </div>
    </div>
  </div>
  );

}

module.exports = {
  view
};
