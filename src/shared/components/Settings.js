/**
 * the Settings menu
 */

const m = require("mithril");

const {MENUSTATES, ACTIONS} = require("../actions.js");

const controller = function(props){
  this.getClasses = function(){
    return [
      "container",
      "menu",
      props.state.settings["threeD"]?"menu--3d":null
    ].join(" ");
  };
  this.dispatchOnCheck = (action) => (event) => props.dispatch({type: action, checked: !!event.target.checked});
};

const view = function(ctrl, props){
  var getCheckbox = function(config, checked){
    if(checked){
      config.checked = checked;
    }else{
      delete config.checked;
    }
    return m("input", config);
  };

  return m("div", {class: ctrl.getClasses()},[
    m("div", {class: "inputElement"},[
      m("label", "enable 3D mode"),
      getCheckbox({type: "checkbox", onchange: ctrl.dispatchOnCheck(ACTIONS.TOGGLE3DMODE)}, props.state.settings.threeD)
    ])
  ]);
};

const component = {
    controller,
    view
};

module.exports = component;
