/**
 * this is the global definition for the logs.
 */


let entries = [];

//---- Levels

let TRACE = 10;
let DEBUG = 20;
let INFO = 30;
let WARN = 40;
let ERROR = 50;

let logThreshold = 20;

function levelString(level) {
  switch (level) {
    case TRACE:
      return "TRACE";
    case DEBUG:
      return "DEBUG";
    case INFO:
      return "INFO";
    case WARN:
      return "WARN";
    case ERROR:
      return "ERROR";
    default:
      return "";
  }
}

function getColor(level) {
  switch (level) {
    case TRACE:
      return "color: #ee8624";
    case DEBUG:
      return "color: #58C294";
    case INFO:
      return "color: #7BAED6";
    case WARN:
      return "color: #ffc83f";
    case ERROR:
      return "color: #fa5e5b";
    default:
      return "color: #666";
  }
}

function argsToArray(args) {
  return [].slice.call(args);
}

function pad(amnt, num) {
  return ("0".repeat(amnt) + num).slice(-2);
}

function logToServer(entry) {

  //we dont have a logserver yet.

  // console.log("send to server");
  // fetch("/log", {
  //     method: "POST",
  //     headers: {
  //       'Accept': 'application/json',
  //       'Content-Type': 'application/json'
  //     },
  //     body: JSON.stringify(entry)
  //   })
  //   .then(function(res) {
  //     if (log) {
  //       log.info("reported error to server:", entry.msg);
  //     }
  //   });
}

function Log(_config) {
  let baseConfig = _config || {
    name: "log"
  };
  let getConfig = function(level) {
    return Object.assign({}, baseConfig, {
      time: new Date(),
      name: baseConfig.name,
      level: level
    });
  };

  function logWithLevel(level, msgs) {
    let config = getConfig(level);
    let entry = Object.assign(config);
    entry.msg = msgs[0];
    entry.data = msgs.slice(1);

    if (entry.msg instanceof Error) {
      entry.msg = entry.msg.message;
      entry.data.unshift(msgs[0].stack);
    }

    entries.push(entry);
    if (entry.level >= WARN) {
      logToServer(entry);
    }

    if(entry.level >= logThreshold){
      let timeString = "[" + entry.time.toISOString() + "]";
      let logFormat = "%s %c%s %c%s";
      let logCommand = console.log.bind(
        console,
        logFormat,
        timeString,
        getColor(level),
        levelString(level),
        getColor(),
        entry.msg);
      entry.data.forEach((datum) => logCommand = logCommand.bind(null, datum));
      logCommand();
    }
  };

  this.trace = function() {
    logWithLevel(TRACE, argsToArray(arguments));
  };
  this.debug = function() {
    logWithLevel(DEBUG, argsToArray(arguments));
  };
  this.info = function() {
    logWithLevel(INFO, argsToArray(arguments));
  };
  this.warn = function() {
    logWithLevel(WARN, argsToArray(arguments));
  };
  this.error = function() {
    logWithLevel(ERROR, argsToArray(arguments));
  };
  this.child = function child(newConfig) {
    return new Log(Object.assign({}, baseConfig, newConfig));
  };
  this.entries = function() {
    return entries;
  };
  this.level = function(level){
    if(typeof level === "string"){
      switch(level){
        case "TRACE":
          return logThreshold = TRACE;
        case "DEBUG":
          return logThreshold = DEBUG;
        case "INFO":
          return logThreshold = INFO;
        case "WARN":
          return logThreshold = WARN;
        case "ERROR":
          return logThreshold = ERROR;
        default:
          return "did not recognize level";
      }
    }
    else{
      logThreshold = parseInt(level) || 0;
    }
  };
};



const log = new Log({
  name: "Division UI"
});
global.log = log;
module.exports = log;
