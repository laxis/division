/**
 * available actions the client can dispatch.
 * MENUSTATES are basically routing actions, while
 * ACTIONS are more of a general, independent type
 */

//module.exports.BUTTON_CLICK = "BUTTON_CLICK"
module.exports.MENUSTATES = Object.freeze({
   MAIN: "/",
   INTEL: "/intel",
   GROUP: "/group",
   MAP: "/map",
   ABILITIES: "/abilities",
   INVENTORY: "/inventory",
   NEWS: "/news",
   SETTINGS: "/settings"
});

module.exports.ACTIONS = Object.freeze({
  TOGGLE3DMODE: "TOGGLE3DMODE",
	GOTO_PARENT: "GOTO_PARENT"
});
