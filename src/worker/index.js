/**
 * this is the entry point for the worker script
 * this will handle ALL actions, manages the state
 * and can (at a later stage) be used for xhr
 */

const m = require("mithril");
const initialState = require("shared/initialState");
const updateState = require("./updateState");
const Socket = require("./socket");
const log = require("shared/log").child({component: "worker"});

//enable mithril requests in worker
m.deps({
  XMLHttpRequest: XMLHttpRequest
});
// let socket;
// Socket("ws://localhost:8080")
// .then(function(_socket){
//   socket = _socket;
//   _socket.addEventListener("message", function(event){
//     let data = JSON.parse(event.data);
//     console.log("socket", data);
//     update({data});
//   });
// })
// .catch(err => console.error(err));

let state = initialState;

function update({data}){
  switch(data._is){
    case "action":
      var newstate = updateState(state, data);
      self.postMessage({_is: "state", state: newstate});
      state = newstate;
      break;
    case "xhr":
      log.trace("worker/REQUEST", data);
      break;
    default:
      break;
  }
}

self.addEventListener("message", update);
self.addEventListener("error", err => console.warn(err));

module.exports.update = update;
