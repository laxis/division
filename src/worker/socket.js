/**
 * socket factory.
 * this should only be used once in the worker index,
 * but who am i to stop you
 */

let sockets = [];

const connect = module.exports = function connect(host){
  return new Promise(function(resolve, reject){
    var socket = new WebSocket(host);
    sockets.push(socket);
    resolve(socket);
  });
};
