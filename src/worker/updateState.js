/**
 * this is the state update function.
 * if you have experience withthe ELM architecture or redux,
 * you should feel right at home.
 */

const {MENUSTATES, ACTIONS} = require("shared/actions");
const parentMenu = require("shared/getParentMenu");
function extendState(state, newstate){
	let newState =  Object.assign({}, state, newstate);
	return newState;
}


function handleAction(state, action){
	switch(action.type){
		case MENUSTATES.SETTINGS:
			return extendState(state, {menustate: MENUSTATES.SETTINGS});
		case MENUSTATES.MAIN:
			return extendState(state, {menustate: MENUSTATES.MAIN});
		case ACTIONS.TOGGLE3DMODE:
			return extendState(state, {
				settings: {
					"threeD": true && action.checked
				}
			});
		case ACTIONS.GOTO_PARENT:
			return extendState(state, {menustate: parentMenu(state.menustate)});
		default:
			return state;
	}
}

module.exports = handleAction;
