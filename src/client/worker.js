/**
 * this is the point, where we create our worker (see worker/index.js).
 * if, for some reason, we dont have access to a worker, we'll create
 * a worker-like alternative that runs in the main thread.
 * 
 * if the worker gets new functionality (next to state and xhr),
 * you probably want to add some convenience functions here.
 */

const Pseudoworker = require("pseudo-worker");
const genid = require("shared/genId");
const initialState = require("shared/initialState");
const workerfile = "js/worker.js";
const log = require("shared/log").child({component: "client/worker"});
let worker;
let currentState = initialState;
let stateListeners = [];
let openRequests = new Map();



if("Worker" in global){
  worker = new Worker(workerfile);
}
else{
  worker = new Pseudoworker(workerfile);
}
worker.addEventListener("message", onNext);
worker.addEventListener("error", onError);

function post(msg){
  worker.postMessage(msg);
}

function onError(event){
  console.error(event);
};
function onNext({data}){
  if(data._is === "state"){
    currentState = Object.freeze(Object.assign({}, data.state));
    log.trace("STATE", currentState);
    stateListeners.map(x => x(currentState));
  }
  else if(data._is === "xhr"){
    if(openRequests.has(data.requestID)){
      log.trace("RESPONSE", data.response, data.requestID);
      openRequests.get(data.requestID)(data.response);
      openRequests.delete(data.requestID);
    }else{
      log.warn("ORPHANED XHR", data.response, data.requestID);
    }
  }
};

function addStateListener(callback){
  stateListeners.push(callback);
  callback(currentState);
}
function removeStateListener(callback){
  stateListeners = stateListeners.filter(x => x !== callback);
}

function doRequest(options){
  return new Promise(function(resolve, reject){
    //random enough
    var requestID = genid();
    log.trace("REQUEST", options, requestID);
    openRequests.set(requestID, [resolve, reject]);
    post({_is:"xhr", options, requestID});
  });
};

function doAction(payload){
  log.trace("ACTION", payload);
  let msg = Object.assign({},payload,{_is: "action"});
  post(msg);
};

const wkr = {
  onState: addStateListener,
  offState: removeStateListener,
  xhr: doRequest,
  dispatch: doAction
};

window.worker = wkr;
module.exports = wkr;
