/**
 * this script attaches a key listener to the document.
 * the aim is to emulate the division keybindings.
 *
 * at some point we should get those keys from the state (for
 * custom keybindings), but for now, we'll stick to vanilla bindings
 */

const worker = require("./worker");
const {ACTIONS, MENUSTATES} = require("shared/actions");
const debounce = require("lodash/debounce");

let keyListener = function(event){
  if(!event.keyCode){ return; };
  switch(event.keyCode){
    case 27: // ESC
      worker.dispatch({type: ACTIONS.GOTO_PARENT});
      break;
    case 73: // I
      worker.dispatch({type: MENUSTATES.INVENTORY});
      break;
    case 76: // L
      worker.dispatch({type: MENUSTATES.ABILITIES});
      break;
    case 77: // M
      worker.dispatch({type: MENUSTATES.MAP});
      break;
    case 79: //O
      worker.dispatch({type: MENUSTATES.GROUP});
      break;
  }
};

if("window" in global){
  global.window.addEventListener("keyup", debounce(keyListener, 50));
}
