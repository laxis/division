/**
 * this is the entry point for the client script.
 * in the client you should aim to only handle ui updates.
 * everything else can and should happen in the worker!
 *
 * due to mithril not having lifecycle events, we do some custom 'routing'.
 *
 * if the states 'menustate' prop changes, it means that we load another
 * top level component (think inventory vs ability).
 * at this point we call the old components 'exit' function (if it has one).
 * this function will get the DOMElement as parameter and is expected
 * to return a Promise.
 * after that we render the new component, execute the new components
 * 'enter' function (again, if applicable), update the URL
 * and go our way.
 *
 * if the component stays the same, we just render the new state with the
 * same component (no exit/enter cycle)
 */

const m = require("mithril");
const worker = require("./worker");
const getComponent = require("shared/getComponent");
const initialState = require("shared/initialState");
const {ACTIONS} = require("shared/actions");
let log = require("shared/log").child({component: "client"});
require("./keyListener");

let rootElement = document.querySelector("main");
let updater = function updater(rootElement, initialState, dispatch){
  function updateUrl(title, newUrl){
    history.pushState(null,title, newUrl);
  }
  let oldState = Object.assign({}, initialState);
  return function updateDOM(state){
    var args = {state, dispatch};
    let newComponent = getComponent(state);
    let exitComponent = oldState.menustate?getComponent(oldState.menustate):{};

    if (state.menustate !== oldState.menustate) {
      log.trace("loading new component");
      Promise.resolve(true)
      .then(function(){
        //handle exit handler;
        return exitComponent.exit?exitComponent.exit(rootElement):true;
      })
      .then(function(){
        return render(rootElement, newComponent, args);
      })
      .then(function(){
        //handle enter handler
        return newComponent.enter?newComponent.enter(rootElement):true;
      })
      .then(function(){
        //update state
        oldState = state;
        updateUrl("DivisionUI",state.menustate);
      })
      .catch(function(err){
        console.error(err);
      });
    }
    else {
      render(rootElement, newComponent, args);
      oldState = state;
    }
  };
};

function render(rootElement, component, args) {
  return new Promise(function(resolve, reject) {
    var ctrl = new component.controller(args);
    var vdom = component.view(ctrl, args);
    m.render(rootElement, vdom);
    resolve();
  });
}

function init() {
  const updateDOM = updater(rootElement, initialState, worker.dispatch);
  worker.onState(updateDOM);
  // if the server gave us an initial state, we want to use that one
  if ("__state" in global){
    updateDOM(global.__state);
  }
}

// bind ready listener
document.addEventListener("readystatechange", function() {
  if (document.readyState === "interactive") {
    init();
  }
});
